const express = require("express")
const uuidv1 = require("uuid/v1")
const axios = require("axios")
const BOT_CODE = "32c4cfa3ca794f37df9a9841f6821237"
const BOT_TOKEN = "4c4687d95d0e75f72b76b5a0ab33738b"

let router = express.Router()

router.get("/", (req,res) => {
    res.render("index.ejs", {uuid: uuidv1()})
})

router.post("/send-messages", async (req,res) => {
    let user_id = req.body.user_id
    let content = req.body.content
    let response = await axios({
        method: "POST",
        url: "https://bot.fpt.ai/api/get_answer/",
        data: {
            "channel": "api",
            "app_code": BOT_CODE,
            "sender_id": user_id,
            "type": "text",
            "message": {
                "content": content,
                "type": "text"
            }
        },
        headers: {
            "Authorization": "Bearer " + BOT_TOKEN
        }
    })
    data = response.data
    console.log(data)
})

module.exports = router