const express = require("express")
const bodyParser = require("body-parser")
const socketIO = require("socket.io")

var app = express()

// use body-parser package
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: true }))

// use controllers
const controllers = require(__dirname + "/apps/controllers")
app.use(controllers)

// set view engine
app.set("views", __dirname + "/apps/views")
app.set("view engine", "ejs")

// set public folder
app.use(express.static(__dirname + "/public"))

// run server
var server = app.listen(3000, (req,res) => {
  console.log("Server is running")
})

var io = socketIO(server)

// add routes
app.post("/webhook", (req,res) => {
  let userId = req.body.user_id
  let content = req.body.messages
  io.to(userId).emit("message", {msg: content})
  res.json({success: true})
})

// set socket events
io.on('connection', (client) => {
    console.log("client connected")

    client.on("user", (data) => {
      client.join(data.user_id)
      console.log("JOIN " + data.user_id)
    })
})